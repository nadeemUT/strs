defmodule StrsWeb.UserController do
  use StrsWeb, :controller

  alias Strs.Accounts
  alias Strs.Accounts.User
  alias Strs.Taxi
  #alias Strs.Taxi


  # defp check_auth(conn, _args) do
  #   if user_id = get_session(conn, :current_user) do
  #     current_user = Accounts.get_user!(user_id)

  #     conn
  #     |> assign(:current_user, current_user)
  #   else
  #     conn
  #     |> put_flash(:error, "You need to signed in to access this page")
  #     |> redirect(to: "/")
  #     |> halt()
  #   end
  # end

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn,user_params) do

    case Accounts.create_user(user_params) do
      {:ok, user} ->
        if user_params["user_type"] == "1" do
            driver_params = %{user_id: user.id, status: true, location: "Narva maantee 25, 51013 Tartu, Estonia"}
            Taxi.create_driver(driver_params)
        end
        put_status(conn, 200)
        |> json(%{msg: "We cannot serve your request in this moment"})
        # |> put_flash(:info, "User created successfully.")
        # |> redirect(to: user_path(conn, :show, user))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end

  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)

    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: user_path(conn, :show, user))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    {:ok, _user} = Accounts.delete_user(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: user_path(conn, :index))
  end
end
