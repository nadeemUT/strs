defmodule Strs.RidesTest do
  use Strs.DataCase

  alias Strs.Rides

  describe "bookings" do
    alias Strs.Rides.Booking

    @valid_attrs %{dropoff: "some dropoff"}
    @update_attrs %{dropoff: "some updated dropoff"}
    @invalid_attrs %{dropoff: nil}

    def booking_fixture(attrs \\ %{}) do
      {:ok, booking} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Rides.create_booking()

      booking
    end

    test "list_bookings/0 returns all bookings" do
      booking = booking_fixture()
      assert Rides.list_bookings() == [booking]
    end

    test "get_booking!/1 returns the booking with given id" do
      booking = booking_fixture()
      assert Rides.get_booking!(booking.id) == booking
    end

    test "create_booking/1 with valid data creates a booking" do
      assert {:ok, %Booking{} = booking} = Rides.create_booking(@valid_attrs)
      assert booking.dropoff == "some dropoff"
    end

    test "create_booking/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Rides.create_booking(@invalid_attrs)
    end

    test "update_booking/2 with valid data updates the booking" do
      booking = booking_fixture()
      assert {:ok, booking} = Rides.update_booking(booking, @update_attrs)
      assert %Booking{} = booking
      assert booking.dropoff == "some updated dropoff"
    end

    test "update_booking/2 with invalid data returns error changeset" do
      booking = booking_fixture()
      assert {:error, %Ecto.Changeset{}} = Rides.update_booking(booking, @invalid_attrs)
      assert booking == Rides.get_booking!(booking.id)
    end

    test "delete_booking/1 deletes the booking" do
      booking = booking_fixture()
      assert {:ok, %Booking{}} = Rides.delete_booking(booking)
      assert_raise Ecto.NoResultsError, fn -> Rides.get_booking!(booking.id) end
    end

    test "change_booking/1 returns a booking changeset" do
      booking = booking_fixture()
      assert %Ecto.Changeset{} = Rides.change_booking(booking)
    end
  end
end
