defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state -> 
    Ecto.Adapters.SQL.Sandbox.checkin(Strs.Repo)
    #Hound.end_session
    nil
  end

  # The skeleton of the steps would be here

  # given_ ~r/^the following taxis are on duty$/, fn state, %{table_data: table} ->
  #   IO.puts "========================================================"
  #   IO.inspect table
  #   IO.puts "========================================================"
  #   table
  #   |> Enum.map(fn taxi -> Taxi.changeset(%Taxi{}, taxi) end)
  #   |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
  #   {:ok, state}
  # end

  given_ ~r/^I open STRS Login page$/, fn state ->
    navigate_to "http://localhost:5000/login"
    {:ok, state}
  end

  and_ ~r/^I enter email and password$/, fn state ->
    fill_field({:name, "email"}, state["test@test.com"] )
    fill_field({:name, "password"}, state["test123"] )
    # {:ok, state}
  end


  when_ ~r/^I summit Login request$/, fn state ->
    click({:id, "login"})
    {:ok, state}
  end

  # then_ ~r/^I should see my profile page$/, fn state ->
  #   assert redirect? "http://localhost:5000/users/1"
  #   {:ok, state}
  # end

end