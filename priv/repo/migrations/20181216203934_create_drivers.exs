defmodule Strs.Repo.Migrations.CreateDrivers do
  use Ecto.Migration

  def change do
    create table(:drivers) do
      add :user_id, :integer
      add :location, :string
      add :status, :boolean, default: false, null: false

      timestamps()
    end

  end
end
