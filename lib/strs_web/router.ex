defmodule StrsWeb.Router do
  use StrsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
  end

  scope "/", StrsWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/users", UserController, :index
    resources "/users", UserController

    get "/login", SessionController, :new
    post "/login", SessionController, :create
    resources "/session", SessionController, only: [:new, :create]

    get "/sessions", SessionController, :delete

    get "/bookings/new", BookingController, :new
    resources "/bookings", BookingController

    resources "/driver", DriverController
  end

  # Other scopes may use custom stacks.
  scope "/api", StrsWeb do
     pipe_through :api
     post "/users", UserController, :create
     resources "/users", UserController
     post "/login", SessionController, :create
     resources "/session", SessionController
     post "/bookings", BookingController, :create
  end
end
