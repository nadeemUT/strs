defmodule Strs.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Strs.Accounts.{User, Encryption}

  schema "users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :password_hash, :string
    field :user_type, :integer
    ## add virtual feilds
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:first_name, :last_name, :email, :password, :user_type])
    |> validate_required([:first_name, :last_name, :email, :password, :user_type])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email, message: "Email should be unique")
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
    |> encrypt_password
  end

  defp encrypt_password(changeset) do
    password = get_change(changeset, :password)

    if password do
      encrypted_password = Encryption.hash_password(password)
      put_change(changeset, :password_hash, encrypted_password)
    else
      changeset
    end
  end
end
