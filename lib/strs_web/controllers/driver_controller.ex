defmodule StrsWeb.DriverController do
  use StrsWeb, :controller

  alias Strs.Taxi
  alias Strs.Taxi.Driver

  def index(conn, _params) do
    drivers = Taxi.list_drivers()
    render(conn, "index.html", drivers: drivers)
  end

  def new(conn, _params) do
    changeset = Taxi.change_driver(%Driver{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"driver" => driver_params}) do
    case Taxi.create_driver(driver_params) do
      {:ok, driver} ->
        conn
        |> put_flash(:info, "Driver created successfully.")
        |> redirect(to: driver_path(conn, :show, driver))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    driver = Taxi.get_driver!(id)
    render(conn, "show.html", driver: driver)
  end

  def edit(conn, %{"id" => id}) do
    driver = Taxi.get_driver!(id)
    changeset = Taxi.change_driver(driver)
    render(conn, "edit.html", driver: driver, changeset: changeset)
  end

  def update(conn, %{"id" => id, "driver" => driver_params}) do
    driver = Taxi.get_driver!(id)

    case Taxi.update_driver(driver, driver_params) do
      {:ok, driver} ->
        conn
        |> put_flash(:info, "Driver updated successfully.")
        |> redirect(to: driver_path(conn, :show, driver))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", driver: driver, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    driver = Taxi.get_driver!(id)
    {:ok, _driver} = Taxi.delete_driver(driver)

    conn
    |> put_flash(:info, "Driver deleted successfully.")
    |> redirect(to: driver_path(conn, :index))
  end
end
