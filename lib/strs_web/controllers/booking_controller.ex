defmodule StrsWeb.BookingController do
  use StrsWeb, :controller

  alias Strs.Rides
  alias Strs.Rides.Booking

  def index(conn, _params) do
    bookings = Rides.list_bookings()
    render(conn, "index.html", bookings: bookings)
  end

  def new(conn, _params) do
    changeset = Rides.change_booking(%Booking{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"booking" => booking_params}) do
    case Rides.create_booking(booking_params) do
      {:ok, booking} ->
        conn
        |> put_flash(:info, "Booking created successfully.")
        |> redirect(to: booking_path(conn, :show, booking))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    booking = Rides.get_booking!(id)
    render(conn, "show.html", booking: booking)
  end

  def edit(conn, %{"id" => id}) do
    booking = Rides.get_booking!(id)
    changeset = Rides.change_booking(booking)
    render(conn, "edit.html", booking: booking, changeset: changeset)
  end

  def update(conn, %{"id" => id, "booking" => booking_params}) do
    booking = Rides.get_booking!(id)

    case Rides.update_booking(booking, booking_params) do
      {:ok, booking} ->
        conn
        |> put_flash(:info, "Booking updated successfully.")
        |> redirect(to: booking_path(conn, :show, booking))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", booking: booking, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    booking = Rides.get_booking!(id)
    {:ok, _booking} = Rides.delete_booking(booking)

    conn
    |> put_flash(:info, "Booking deleted successfully.")
    |> redirect(to: booking_path(conn, :index))
  end
end
