defmodule Strs.Repo.Migrations.CreateNewUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :user_type, :integer
    end

  end
end
