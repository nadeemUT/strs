defmodule Strs.Repo.Migrations.CreateBookings do
  use Ecto.Migration

  def change do
    create table(:bookings) do
      add :dropoff, :string

      timestamps()
    end

  end
end
