use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :strs, StrsWeb.Endpoint,
  http: [port: 5001],
  #server: false
  server: true

# Print only warnings and errors during test
config :logger, level: :warn
config :hound, driver: "chrome_driver"
config :strs, sql_sandbox: true
# Configure your database
config :strs, Strs.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
 # password: "postgres",
 password: "test123",
  #database: "strs_test",
  database: "strs_dev",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
