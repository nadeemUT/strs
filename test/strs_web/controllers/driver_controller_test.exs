defmodule StrsWeb.DriverControllerTest do
  use StrsWeb.ConnCase

  alias Strs.Taxi

  @create_attrs %{location: "some location", status: true, user_id: 42}
  @update_attrs %{location: "some updated location", status: false, user_id: 43}
  @invalid_attrs %{location: nil, status: nil, user_id: nil}

  def fixture(:driver) do
    {:ok, driver} = Taxi.create_driver(@create_attrs)
    driver
  end

  describe "index" do
    test "lists all drivers", %{conn: conn} do
      conn = get conn, driver_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Drivers"
    end
  end

  describe "new driver" do
    test "renders form", %{conn: conn} do
      conn = get conn, driver_path(conn, :new)
      assert html_response(conn, 200) =~ "New Driver"
    end
  end

  describe "create driver" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, driver_path(conn, :create), driver: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == driver_path(conn, :show, id)

      conn = get conn, driver_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Driver"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, driver_path(conn, :create), driver: @invalid_attrs
      assert html_response(conn, 200) =~ "New Driver"
    end
  end

  describe "edit driver" do
    setup [:create_driver]

    test "renders form for editing chosen driver", %{conn: conn, driver: driver} do
      conn = get conn, driver_path(conn, :edit, driver)
      assert html_response(conn, 200) =~ "Edit Driver"
    end
  end

  describe "update driver" do
    setup [:create_driver]

    test "redirects when data is valid", %{conn: conn, driver: driver} do
      conn = put conn, driver_path(conn, :update, driver), driver: @update_attrs
      assert redirected_to(conn) == driver_path(conn, :show, driver)

      conn = get conn, driver_path(conn, :show, driver)
      assert html_response(conn, 200) =~ "some updated location"
    end

    test "renders errors when data is invalid", %{conn: conn, driver: driver} do
      conn = put conn, driver_path(conn, :update, driver), driver: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Driver"
    end
  end

  describe "delete driver" do
    setup [:create_driver]

    test "deletes chosen driver", %{conn: conn, driver: driver} do
      conn = delete conn, driver_path(conn, :delete, driver)
      assert redirected_to(conn) == driver_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, driver_path(conn, :show, driver)
      end
    end
  end

  defp create_driver(_) do
    driver = fixture(:driver)
    {:ok, driver: driver}
  end
end
