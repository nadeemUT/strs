defmodule Strs.Rides.Booking do
  use Ecto.Schema
  import Ecto.Changeset


  schema "bookings" do
    field :dropoff, :string
    field :pickup, :string
    field :userid, :integer
    field :booked_at, :string
    field :status, :string, default: "pending"

    timestamps()
  end

  @doc false
  def changeset(booking, attrs) do
    booking
    |> cast(attrs, [:pickup, :dropoff])
    |> validate_required([:pickup, :dropoff])

  end
end
