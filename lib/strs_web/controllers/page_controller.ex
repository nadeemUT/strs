defmodule StrsWeb.PageController do
  use StrsWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
