defmodule StrsWeb.SessionController do
  use StrsWeb, :controller
  import Plug.Conn
  alias Strs.Repo
  alias Strs.Accounts.{User, Auth}

  def index(conn, _params) do
    conn |> redirect(to: "/login")
  end

  def new(conn, _params) do
      render conn, "new.html"
  end

  def create(conn, session_params) do

    case Auth.login(session_params, Repo) do
      {:ok, user} ->
        conn
        |> put_session(:current_user, user.id)
        |> put_flash(:info, "Logged in successfully.")
        |> redirect(to: "/users/#{user.id}")
      {:error}  ->
        conn |> json(%{msg: "We cannot serve your request in this moment"})
    end
  end

  def delete(conn, _) do
    conn
    |> delete_session(:current_user)
    |> put_flash(:info, "Logged out successfully")
    |> redirect(to: "/login")
  end
end
