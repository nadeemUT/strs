defmodule Strs.Taxi.Driver do
  use Ecto.Schema
  import Ecto.Changeset


  schema "drivers" do
    field :location, :string
    field :status, :boolean, default: true
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(driver, attrs) do
    driver
    |> cast(attrs, [:user_id, :location, :status])
    |> validate_required([:user_id, :location, :status])
  end
end
