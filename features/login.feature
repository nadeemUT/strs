Feature: Login
  As a customer
  I want to Login in to the STRS APP
  such that I can go to my profile page

  Scenario: Login via STRS Login web page
        
    Given I open STRS Login page
    And I enter the email and password
    When I submit the Login request
    Then I should see my profile page

    