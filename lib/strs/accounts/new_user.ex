defmodule Strs.Accounts.NewUser do
  use Ecto.Schema
  import Ecto.Changeset


  schema "new_users" do
    field :user_type, :integer

    timestamps()
  end

  @doc false
  def changeset(new_user, attrs) do
    new_user
    |> cast(attrs, [:user_type])
    |> validate_required([:user_type])
  end
end
