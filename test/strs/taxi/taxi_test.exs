defmodule Strs.TaxiTest do
  use Strs.DataCase

  alias Strs.Taxi

  describe "drivers" do
    alias Strs.Taxi.Driver

    @valid_attrs %{location: "some location", status: true, user_id: 42}
    @update_attrs %{location: "some updated location", status: false, user_id: 43}
    @invalid_attrs %{location: nil, status: nil, user_id: nil}

    def driver_fixture(attrs \\ %{}) do
      {:ok, driver} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Taxi.create_driver()

      driver
    end

    test "list_drivers/0 returns all drivers" do
      driver = driver_fixture()
      assert Taxi.list_drivers() == [driver]
    end

    test "get_driver!/1 returns the driver with given id" do
      driver = driver_fixture()
      assert Taxi.get_driver!(driver.id) == driver
    end

    test "create_driver/1 with valid data creates a driver" do
      assert {:ok, %Driver{} = driver} = Taxi.create_driver(@valid_attrs)
      assert driver.location == "some location"
      assert driver.status == true
      assert driver.user_id == 42
    end

    test "create_driver/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Taxi.create_driver(@invalid_attrs)
    end

    test "update_driver/2 with valid data updates the driver" do
      driver = driver_fixture()
      assert {:ok, driver} = Taxi.update_driver(driver, @update_attrs)
      assert %Driver{} = driver
      assert driver.location == "some updated location"
      assert driver.status == false
      assert driver.user_id == 43
    end

    test "update_driver/2 with invalid data returns error changeset" do
      driver = driver_fixture()
      assert {:error, %Ecto.Changeset{}} = Taxi.update_driver(driver, @invalid_attrs)
      assert driver == Taxi.get_driver!(driver.id)
    end

    test "delete_driver/1 deletes the driver" do
      driver = driver_fixture()
      assert {:ok, %Driver{}} = Taxi.delete_driver(driver)
      assert_raise Ecto.NoResultsError, fn -> Taxi.get_driver!(driver.id) end
    end

    test "change_driver/1 returns a driver changeset" do
      driver = driver_fixture()
      assert %Ecto.Changeset{} = Taxi.change_driver(driver)
    end
  end
end
